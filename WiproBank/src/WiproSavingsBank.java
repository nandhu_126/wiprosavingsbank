
public class WiproSavingsBank {
String accountNo;
String name;
int age;
String address;
double available_balance;
double min_balance;
public WiproSavingsBank(String accountNo,String name, int age, String address, double available_balance, double min_balance) {
	super();
	this.accountNo=accountNo;
	this.name = name;
	this.age = age;
	this.address = address;
	this.available_balance = available_balance;
	this.min_balance = min_balance;
}



@Override
public String toString() {
	return "WiproSavingsBank [accountNo=" + accountNo + ", name=" + name + ", age=" + age + ", address=" + address
			+ ", available_balance=" + available_balance + ", min_balance=" + min_balance + "]";
}



public double deposit(int amount){
	available_balance=this.available_balance+amount;
	return available_balance;
	
}
public double withdrawl(int amount){
	if(available_balance>=amount){
		return available_balance=available_balance-amount;
	}
	else{
		return this.min_balance;
	}
	}
public void balEnquiry(String accountno){
	if(this.accountNo.equals(accountno)){
		System.out.println(toString());
	}
	else{
		System.out.println("Account number doesnot match");
	}
}
	public static void main(String[] args) {
		WiproSavingsBank ws=new WiproSavingsBank("1034256","nandhu",21,"TNagar",24000,1000);
		WiproSavingsBank ws1=new WiproSavingsBank("1025626","hari",22,"Adayar",400,1000);
        ws.deposit(3000);
        ws.withdrawl(1000);
        ws.balEnquiry("1034256");
        ws1.deposit(200);
        ws1.withdrawl(500);
        ws1.balEnquiry("1025636");
	}

}
